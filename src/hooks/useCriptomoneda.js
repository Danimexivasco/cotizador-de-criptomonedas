import React, { Fragment, useState } from 'react';
import styled from '@emotion/styled';



const Label = styled.label`
    font-family: 'Bebas Neue', cursive;
    color: #FFF;
    text-transform:uppercase;
    font-size:2.2rem;
    margin-top:2rem;
    display:block;
    font-weight: bold;
`;

const Select = styled.select`
    width:100%;
    display:block;
    padding:1rem;
    -webkit-appearance: none;
    border-radius: 10px;
    border:none;
    font-size:1.2rem;
    text-align:center;
    margin-top:0.5rem;
    &:hover{
        cursor:pointer;
    }
`;

const useCriptomoneda = (label, stateInicial, opciones) => {

    // state de cripto
    const [State, setState] = useState(stateInicial);


    // Lo que se mostrará en pantalla
    const seleccionarCripto = () => (
        <Fragment>
            <Label>{label}</Label>
            <Select
            onChange={e=>setState(e.target.value)}
            value={State}
            >
                <option value="" disabled>-- Selecciona -- </option>
               {opciones.map(opcion=>{
                   return (
                    <option key ={opcion.CoinInfo.Id} value={opcion.CoinInfo.Name} >{opcion.CoinInfo.FullName}</option>
                       
                   )
               })}

            </Select>
        </Fragment>
    )

    return [State, seleccionarCripto, setState];
}

export default useCriptomoneda;