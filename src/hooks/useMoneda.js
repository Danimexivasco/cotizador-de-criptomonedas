import React, { Fragment, useState } from 'react';
import styled from '@emotion/styled';

const Label = styled.label`
    font-family: 'Bebas Neue', cursive;
    color: #FFF;
    text-transform:uppercase;
    font-size:2.2rem;
    margin-top:2rem;
    display:block;
    font-weight: bold;
`;

const Select = styled.select`
    width:100%;
    display:block;
    padding:1rem;
    -webkit-appearance: none;
    border-radius: 10px;
    border:none;
    font-size:1.2rem;
    text-align:center;
    margin-top:0.5rem;
    &:hover{
        cursor:pointer;
    }
`;

const useMoneda = (label, stateInicial, opciones) => {

    // state de moneda
    const [Moneda, setMoneda] = useState(stateInicial);

    // Lo que se mostrará en pantalla
    const Seleccionar = () => (
        <Fragment>
            <Label>{label}</Label>
            <Select
            onChange={e=>setMoneda(e.target.value)}
            value={Moneda}
            >
                <option value="" disabled>-- Selecciona -- </option>
                {opciones.map(opcion => {
                    return (<option key={opcion.codigo} value={opcion.codigo}>{opcion.nombre}</option>)
                })}

            </Select>
        </Fragment>
    )

    return [Moneda, Seleccionar, setMoneda];
}

export default useMoneda;