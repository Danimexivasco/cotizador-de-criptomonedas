import React from 'react';
import styled from '@emotion/styled';


const MensajeError = styled.p`
    background-color: #b7322c;
    font-size: 30px;
    color:#FFF;
    text-align:center;
    font-weight:bold;
    padding:1rem;
    font-family: 'Bebas Neue', cursive;
`;

const Error = ({msg}) => {
    return ( 
        <MensajeError> {msg}</MensajeError>
     );
}
 
export default Error;