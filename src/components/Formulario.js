import React, { useState, useEffect } from 'react';
import styled from '@emotion/styled';
import useMoneda from '../hooks/useMoneda'
import useCriptomoneda from '../hooks/useCriptomoneda';
import axios from 'axios';
import Error from './Error'

const Boton = styled.input`
    margin-top: 20px;
    font-weight: bold;
    font-size: 20px;
    padding: 10px;
    background-color: #66a2fe;
    border:none;
    width: 100%;
    border-radius: 10px;
    color: #FFF;
    transition: background-color .3s ease;

&:hover{
    background-color:#326AC0;
    cursor:pointer;
}
`;



const Formulario = ({guardarMoneda, guardarCriptomoneda, guardarMostrarResultado}) => {

    useEffect(() => {
        const llamada = async () => {
            const url = 'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD'

            const resultado = await axios.get(url)

            guardarListadoAPI(resultado.data.Data)
        }
        llamada();
    }, [])

    const Monedas = [
        { codigo: 'USD', nombre: 'US Dolar' },
        { codigo: 'MXN', nombre: 'Peso Mexicano' },
        { codigo: 'EUR', nombre: 'Euro' },
        { codigo: 'GBP', nombre: 'Libra Esterlina' }
    ]

     // State del listado que nos proporciona la API
     const [listadoAPI, guardarListadoAPI] = useState([])

     // Nos traemos el state de useMoneda
     const [Moneda, Seleccionar] = useMoneda('Selecciona una Moneda', '', Monedas)
 
     const [Criptomoneda, SeleccionarCripto] = useCriptomoneda('Eligue tu criptomoneda', '', listadoAPI)
 
     // State de errores 
     const [error, guardarError] = useState(false)


    const cotizarMoneda = e => {
        e.preventDefault();

        // Validamos el form
        if (Moneda.trim() === '' || Criptomoneda.trim() === '') {
            guardarError(true)
            return;
        }
        guardarError(false);
        guardarMoneda(Moneda);
        guardarCriptomoneda(Criptomoneda);
        // guardarMostrarResultado(true)
    }

    return (
        <form
            onSubmit={cotizarMoneda}
        >
            {error ? <Error msg="Todos los campos son obligatorios" /> : null}
            <Seleccionar />

            <SeleccionarCripto />

            <Boton
                type="submit"
                value="Cotiza"
            />

        </form>

    );
}

export default Formulario;