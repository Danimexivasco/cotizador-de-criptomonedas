import React from 'react';
import styled from '@emotion/styled';



const DivContenido = styled.div`
    color: #FFF;
    font-family: Arial, Helvetica, sans-serif;
`;

const Precio = styled.p`
    font-size: 30px;
    span{
        font-weight: bold;
    }
`;

const Info = styled.p`
    font-size: 18px;
    span{
        font-weight: bold;
    }
`;
const Resultado = ({resultado}) => {
    
    // Otra forma para evitar que se muestre nada antes de que se seleccione algo
    // if(Object.keys(resultado).length === 0) return null;

    return ( 

        <DivContenido>
            <Precio>El valor actual es: <span>{resultado.PRICE}</span></Precio>
            <Info>El valor más alto del día ha sido: <span>{resultado.HIGHDAY}</span></Info>
            <Info>El valor más bajo del día ha sido: <span>{resultado.LOWDAY}</span></Info>
            <Info>Variación últimas 24H: <span>{resultado.CHANGEPCT24HOUR}</span></Info>
            <Info>Última actualización: <span>{resultado.LASTUPDATE}</span></Info>

        </DivContenido>

     );
}
 
export default Resultado;