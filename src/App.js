import React, { useState, useEffect } from 'react';
import styled from '@emotion/styled';
import imagen from './cryptomonedas.png';
import axios from 'axios';
import Formulario from './components/Formulario';
import Resultado from './components/Resultado';
import Spinner from './components/Spinner';


const Contenedor = styled.div`
  max-width:900px;
  margin: 0 auto;
@media (min-width:992px){
  display:grid;
  grid-template-columns: repeat(2,1fr);
  column-gap: 2rem;
}
`;

const Imagen = styled.img`
  max-width:100%;
  margin-top:5rem;
`;

const Heading = styled.h1`
  font-family:'Bebas Neue', cursive;
  color: #FFF;
  text-align:left;
  font-weight:700;
  font-size:50px;
  margin-bottom: 50px;
  margin-top:80px;

  &::after{
    content: '';
    height:6px;
    background-color:#66A2FE;
    display:block;

  }
`;

function App() {

  // State de Criptomonedas y useMoneda, para posteriormente pasarlo al resultado
  const [moneda, guardarMoneda] = useState('');

  const [criptomoneda, guardarCriptomoneda] = useState('');

  const [resultado, guardarResultado] = useState({})

  const [mostrarResultado, guardarMostrarResultado] = useState(false)

  const [cargando, guardarCargando] = useState(false)

  useEffect(() => {

    const consultarCriptomoneda = async () => {
      // Evitamos que se ejecute la primera vez
      if (moneda === '') return;

      // Llamada a la API
      const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptomoneda}&tsyms=${moneda}`;

      const respuesta = await axios.get(url);

      // Cambiamos state para el spinner
      guardarCargando(true)

      setTimeout(() => {
        // Pasados lo 2-3 segundos pasamos el state a false de nuevo
        guardarCargando(false);
        // Nos traemos la info
        guardarResultado(respuesta.data.DISPLAY[criptomoneda][moneda])
        guardarMostrarResultado(true)
      }, 2500)
    }
    consultarCriptomoneda();
  }, [moneda, criptomoneda])


  const componente = (cargando) ? <Spinner /> : ((mostrarResultado) ?
    <Resultado
      resultado={resultado}
    />
    :
    null)


  return (
    <Contenedor>
      <div>
        <Imagen
          src={imagen}
          alt="Imagen Crypto"
        />
      </div>

      <div>
        <Heading>Cotiza Cryptomonedas al Instante</Heading>
        <Formulario
          guardarCriptomoneda={guardarCriptomoneda}
          guardarMoneda={guardarMoneda}
          guardarMostrarResultado={guardarMostrarResultado}
        />


        {componente}
       


      </div>
    </Contenedor>
  );
}

export default App;
